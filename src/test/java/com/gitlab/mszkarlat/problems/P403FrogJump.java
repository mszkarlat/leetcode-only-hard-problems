package com.gitlab.mszkarlat.problems;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.Arguments.of;

/**
 * A frog is crossing a river. The river is divided into some number of units, and at each unit, there may or may not
 * exist a stone. The frog can jump on a stone, but it must not jump into the water.
 * <p>
 * Given a list of stones positions (in units) in sorted ascending order, determine if the frog can cross the river by
 * landing on the last stone. Initially, the frog is on the first stone and assumes the first jump must be 1 unit.
 * <p>
 * If the frog's last jump was k units, its next jump must be either k - 1, k, or k + 1 units. The frog can only jump in
 * the forward direction.>
 * <p>
 * Example 1:
 * Input: stones = [0,1,3,5,6,8,12,17]
 * Output: true
 * Explanation: The frog can jump to the last stone by jumping 1 unit to the 2nd stone, then 2 units to the 3rd stone,
 * then 2 units to the 4th stone, then 3 units to the 6th stone, 4 units to the 7th stone, and 5 units to the 8th stone.
 * <p>
 * Example 2:
 * Input: stones = [0,1,2,3,4,8,9,11]
 * Output: false
 * Explanation: There is no way to jump to the last stone as the gap between the 5th and 6th stone is too large.
 * <p>
 * Constraints:
 * 2 <= stones.length <= 2000
 * 0 <= stones[i] <= 2^31 - 1
 * stones[0] == 0
 * stones is sorted in a strictly increasing order.
 */
public class P403FrogJump {
    // time: O(n^2)
    // space: O(n^2)
    public boolean canCross(int[] stones) {
        final var size = stones.length;
        // matrix[A][B] with checks 'can jump to stone A with the jump of size B?'
        final var possibleJumps = new boolean[size][size];

        for (int i = size - 1; i > 0; i--) {

            for (int j = size - 1; j > i - 1; j--) {
                final var jumpSize = stones[j] - stones[i - 1];

                if (jumpSize > i) {
                    continue;
                }

                if (j == size - 1) {
                    possibleJumps[i - 1][jumpSize] = true;
                } else if (possibleJumps[j][jumpSize - 1] || possibleJumps[j][jumpSize] || possibleJumps[j][jumpSize + 1]) {
                    possibleJumps[i - 1][jumpSize] = true;
                }
            }
        }

        return possibleJumps[0][1];
    }

    static Stream<Arguments> trivialExamples() {
        return Stream.of(
                of(new int[]{0, 1, 3, 5, 6, 8, 12, 17}, true),
                of(new int[]{0, 1, 3, 6, 7}, false),
                of(new int[]{0, 1, 3, 6, 10, 13, 15, 16, 19, 21, 25}, false),
                of(new int[]{0, 1}, true),
                of(new int[]{0, 1, 3, 6, 10, 13, 14}, true),
                of(new int[]{0, 2}, false),
                of(new int[]{0, 1, 6, 12}, false),
                of(new int[]{0, 1, 2, 3, 4, 8, 9, 11}, false));
    }

    @ParameterizedTest
    @MethodSource("trivialExamples")
    public void shouldPassTrivialExamples(int[] stones, boolean expected) {
        // When
        final var actual = canCross(stones);

        // Then
        assertThat(actual).isEqualTo(expected);
    }
}
