package com.gitlab.mszkarlat.problems;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.Arguments.of;

/**
 * Given two sorted arrays nums1 and nums2 of size m and n respectively, return the median of the two sorted arrays.
 * The overall run time complexity should be O(log (m+n)).
 * <p>
 * Example 1:
 * Input: nums1 = [1,3], nums2 = [2]
 * Output: 2.00000
 * Explanation: merged array = [1,2,3] and median is 2.
 * <p>
 * Example 2:
 * Input: nums1 = [1,2], nums2 = [3,4]
 * Output: 2.50000
 * Explanation: merged array = [1,2,3,4] and median is (2 + 3) / 2 = 2.5.
 * <p>
 * Constraints:
 * nums1.length == m
 * nums2.length == n
 * 0 <= m <= 1000
 * 0 <= n <= 1000
 * 1 <= m + n <= 2000
 * -106 <= nums1[i], nums2[i] <= 106
 */
public class P4MedianOfTwoSortedArrays {
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        final var size = nums1.length + nums2.length;

        if (size % 2 != 0) {
            // median is the index of 'size/2'
            return Math.max(getNthElementOf(nums1, nums2, size / 2), getNthElementOf(nums2, nums1, size / 2));

        } else {
            // median is the average of the indexes 'size/2' and 'size/2 - 1'
            final var i = Math.max(getNthElementOf(nums1, nums2, size / 2), getNthElementOf(nums2, nums1, size / 2));
            final var j = Math.max(getNthElementOf(nums1, nums2, size / 2 - 1), getNthElementOf(nums2, nums1, size / 2 - 1));

            return (i + j) / 2.0d;
        }
    }

    // O(logN1 * logN2)
    // if 'nums1' has the Nth element from the merged arrays, returns it. Otherwise, returns Integer.MIN_VALUE
    private int getNthElementOf(int[] nums1, int[] nums2, int n) {
        int start = 0, end = nums1.length;
        int curr = (end - start) / 2;

        while (start != end) {
            final var lowIndex = getIndexOf(nums2, nums1[curr]);
            final var highIndex = curr + 1 < nums1.length ? getIndexOf(nums2, nums1[curr + 1]) : getIndexOf(nums2, nums1[curr] + 1);

            final int possibleNthStar = lowIndex + curr + 1;
            final var possibleNthStop = highIndex + curr + 1;

            if (possibleNthStar <= n && n <= possibleNthStop) {
                return nums1[curr];

            } else if (possibleNthStop < n) {
                start = curr + 1;
                curr = end - (end - curr) / 2;

            } else {
                end = curr;
                curr = start + (curr - start) / 2;
            }
        }

        return Integer.MIN_VALUE;
    }

    // O(logN)
    // returns the highest index that satisfies 'arr[index] < value'
    private int getIndexOf(int[] arr, int value) {
        int start = 0, end = arr.length;
        int curr = end / 2;

        while (start != end) {
            if (arr[curr] < value) {
                start = curr + 1;
                curr = end - (end - curr) / 2;

            } else {
                end = curr;
                curr = start + (curr - start) / 2;
            }
        }

        return curr - 1;
    }

    static Stream<Arguments> trivialExamples() {
        return Stream.of(
                of(new int[]{1}, new int[]{1}, 1d),
                of(new int[]{2, 2, 4, 4}, new int[]{2, 2, 4, 4}, 3d),
                of(new int[]{1, 8, 14}, new int[]{2, 8, 8, 9, 16, 17, 23, 30, 38}, 11.5d),
                of(new int[]{1, 3}, new int[]{2}, 2d),
                of(new int[]{1, 2, 3, 4, 5}, new int[]{}, 3d),
                of(new int[]{1, 2}, new int[]{3, 4}, 2.5d));
    }

    @ParameterizedTest
    @MethodSource("trivialExamples")
    public void shouldFindMedian(int[] nums1, int[] nums2, double expected) {
        // When
        final var actual = findMedianSortedArrays(nums1, nums2);

        // Then
        assertThat(actual).isEqualTo(expected);
    }

    static Stream<Arguments> indexOfValues() {
        return Stream.of(
                of(new int[]{2, 8, 8, 9, 11, 11, 11, 20}, 13, 6),
                of(new int[]{2, 8, 8, 9, 16, 17, 23, 30, 38}, 0, -1),
                of(new int[]{2, 8, 8, 9, 16, 17, 23, 30, 38}, 100, 8),
                of(new int[]{2, 8, 8, 9, 16, 17, 23, 30, 38}, 30, 6),
                of(new int[]{2, 8, 8, 9, 16, 17, 23, 30, 38}, 15, 3),
                of(new int[]{2, 8, 8, 9, 16, 17, 23, 30, 38}, 9, 2),
                of(new int[]{2, 8, 8, 9, 16, 17, 23, 30, 38}, 8, 0));
    }

    @ParameterizedTest
    @MethodSource("indexOfValues")
    public void shouldFindIndexOfValue(int[] nums, int value, int expected) {
        // When
        final var actual = getIndexOf(nums, value);

        // Then
        assertThat(actual).isEqualTo(expected);
    }
}
