package com.gitlab.mszkarlat.problems;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.Map.entry;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.Arguments.of;

/**
 * Given an input string s and a pattern p, implement regular expression matching with support for '.' and '*' where:
 * '.' Matches any single character.
 * '*' Matches zero or more of the preceding element.
 * The matching should cover the entire input string (not partial).
 * <p>
 * Example 1:
 * Input: s = "aa", p = "a"
 * Output: false
 * Explanation: "a" does not match the entire string "aa".
 * <p>
 * Example 2:
 * Input: s = "aa", p = "a*"
 * Output: true
 * Explanation: '*' means zero or more of the preceding element, 'a'. Therefore, by repeating 'a' once, it becomes "aa".
 * <p>
 * Example 3:
 * Input: s = "ab", p = ".*"
 * Output: true
 * Explanation: ".*" means "zero or more (*) of any character (.)".
 * <p>
 * Constraints:
 * 1 <= s.length <= 20
 * 1 <= p.length <= 20
 * s contains only lowercase English letters.
 * p contains only lowercase English letters, '.', and '*'.
 * It is guaranteed for each appearance of the character '*', there will be a previous valid character to match.
 */
public class P10RegularExpressionMatching {
    public static final char ANY_CHAR = '.';
    public static final char STAR_CHAR = '*';

    public enum Token {CHAR_SEQUENCE, STAR_TOKEN, ANY_TOKEN}

    public record PatternRecord(Token token, char sequence) {
        public PatternRecord(char sequence) {
            this(sequence == ANY_CHAR ? Token.ANY_TOKEN : Token.CHAR_SEQUENCE, sequence);
        }
    }

    public boolean isMatch(String s, String p) {
        final var tokens = tokenize(p);
        final Deque<Entry<String, List<PatternRecord>>> deque = new ArrayDeque<>(p.length() * p.length());
        final Set<Entry<String, List<PatternRecord>>> visited = new HashSet<>();

        deque.add(entry(s, tokens));
        visited.add(entry(s, tokens));

        while (!deque.isEmpty()) {
            final var entry = deque.pop();
            final var check = isMatch(entry.getKey(), entry.getValue(), deque, visited);

            if (check) {
                return true;
            }
        }

        return false;
    }

    private boolean isMatch(String s, List<PatternRecord> patterns, Deque<Entry<String, List<PatternRecord>>> deque, Set<Entry<String, List<PatternRecord>>> visited) {
        final var input = new StringBuilder(s);
        for (int i = 0; i < patterns.size(); i++) {
            final var record = patterns.get(i);

            if (record.token() != Token.STAR_TOKEN) {
                if (input.isEmpty() || !(record.token() == Token.ANY_TOKEN || input.charAt(0) == record.sequence())) {
                    return false;
                } else {
                    input.delete(0, 1);
                }

            } else {
                final var remainingPattern = patterns.subList(i + 1, patterns.size());
                final var remainingCharacters = input.length() - remainingPattern.stream()
                        .filter(tokenRecord -> tokenRecord.token() != Token.STAR_TOKEN)
                        .count();

                for (int j = 0; j < remainingCharacters + 1; j++) {
                    final var candidate = entry(input.toString(), Stream.concat(
                                    new StringBuilder(j).repeat(record.sequence(), j).chars()
                                            .mapToObj(c -> new PatternRecord((char) c)),
                                    patterns.subList(i + 1, patterns.size()).stream())
                            .toList());

                    if (visited.add(candidate)) {
                        deque.add(candidate);
                    }
                }

                return false;
            }
        }

        return input.isEmpty();
    }

    private List<PatternRecord> tokenize(String p) {
        if (p.length() == 1) {
            return Collections.singletonList(new PatternRecord(p.charAt(0)));
        }

        final var tokens = Stream.<PatternRecord>builder();
        for (int i = 0; i < p.length(); i++) {
            if (i + 1 < p.length() && p.charAt(i + 1) == STAR_CHAR) {
                // '.*' or 'a*'
                tokens.accept(new PatternRecord(Token.STAR_TOKEN, p.charAt(i)));
                i++;
            } else {
                // 'aa' or 'a.' or '..' or '.a'
                tokens.accept(new PatternRecord(p.charAt(i)));
            }
        }

        return tokens.build().toList();
    }

    static Stream<Arguments> trivialExamples() {
        return Stream.of(
                of("bbbba", ".*a*a", true),
                of("aaaaaaaaaaaaa", "a*a*a*a*a*a*a*a*a*a*a", true),
                of("aa", "a", false),
                of("...aa", "fgsaa", false),
                of("trąba", "trąba", true),
                of("aa", "a*", true),
                of("b", "a*", false),
                of("b", "a*b", true),
                of("b", ".*b", true),
                of("b", "a*b*c*d*", true),
                of("", "a*", true),
                of("aa", ".*", true),
                of("aaaaab", ".*b", true),
                of("michal szkarlat", ".* szkar*la.", true),
                of("michal szkalat", ".* szkar*la.", true),
                of("aaaaaaaaaaaaab", "a*a*a*a*a*a*a*a*a*c", false),
                of("aaaaaaaaaaaaaaaaaaab", "a*a*a*a*a*a*a*a*a*a*", false));
    }

    @ParameterizedTest
    @MethodSource("trivialExamples")
    public void shouldPassTrivialExamples(String s, String p, boolean expected) {
        // When
        final var actual = isMatch(s, p);

        // Then
        assertThat(actual).isEqualTo(expected);
    }
}
