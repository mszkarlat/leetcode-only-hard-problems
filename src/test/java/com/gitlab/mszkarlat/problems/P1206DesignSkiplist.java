package com.gitlab.mszkarlat.problems;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Objects;
import java.util.Random;
import java.util.stream.Stream;

import static java.util.Arrays.stream;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.Arguments.of;

/**
 * Design a Skiplist without using any built-in libraries.
 * A skiplist is a data structure that takes O(log(n)) time to add, erase and search.
 * Comparing with treap and red-black tree which has the same function and performance,
 * the code length of Skiplist can be comparatively short and the idea behind Skiplists is just simple linked lists.
 * <p>
 * Implement the Skiplist class:
 * Skiplist() Initializes the object of the skiplist.
 * bool search(int target) Returns true if the integer target exists in the Skiplist or false otherwise.
 * void add(int num) Inserts the value num into the SkipList.
 * bool erase(int num) Removes the value num from the Skiplist and returns true. If num does not exist in the Skiplist,
 * do nothing and return false. If there exist multiple num values, removing any one of them is fine.
 * Note that duplicates may exist in the Skiplist, your code needs to handle this situation.
 * <p>
 * Example 1:
 * Input
 * ["Skiplist", "add", "add", "add", "search", "add", "search", "erase", "erase", "search"]
 * [[], [1], [2], [3], [0], [4], [1], [0], [1], [1]]
 * Output
 * [null, null, null, null, false, null, true, false, true, false]
 * Explanation
 * Skiplist skiplist = new Skiplist();
 * skiplist.add(1);
 * skiplist.add(2);
 * skiplist.add(3);
 * skiplist.search(0); // return False
 * skiplist.add(4);
 * skiplist.search(1); // return True
 * skiplist.erase(0);  // return False, 0 is not in skiplist.
 * skiplist.erase(1);  // return True
 * skiplist.search(1); // return False, 1 has already been erased.
 * <p>
 * Constraints:
 * 0 <= num, target <= 2 * 1^04
 * At most 5 * 10^4 calls will be made to search, add, and erase.
 */
public class P1206DesignSkiplist {
    public static class Skiplist {
        private record Node(int value, Node[] next) {
        }

        private static final int MAX_SIZE = 5 * 10000;
        private static final int MAX_LEVEL = (int) (Math.log(MAX_SIZE) / Math.log(2)) + 1;
        private final Random random;
        private final Node[] guard;
        private int currentLevel;

        public Skiplist() {
            this.random = new Random();
            this.guard = new Node[MAX_LEVEL];
            this.currentLevel = 0;
        }

        public boolean search(int target) {
            final var curr = getNodeOf(target, 1);

            return curr[0] != null && (curr[0].value() == target || curr[0].next()[0] != null && curr[0].next()[0].value() == target);
        }

        public void add(int num) {
            final var node = new Node(num, new Node[getLevel()]);
            final var curr = getNodeOf(num, node.next().length);

            for (int i = curr.length - 1; i >= 0; i--) {
                if (curr[i] == null || curr[i].value() > num) {
                    node.next()[i] = guard[i];
                    guard[i] = node;
                } else {
                    node.next()[i] = curr[i].next()[i];
                    curr[i].next()[i] = node;
                }
            }

            if (node.next().length > currentLevel) {
                currentLevel = node.next().length;
            }
        }

        public boolean erase(int num) {
            final var curr = getNodeOf(num, currentLevel);

            if (curr[0] == null || (curr[0].value() != num && (curr[0].next()[0] == null || curr[0].next()[0].value() != num))) {
                return false;
            }

            for (int i = curr.length - 1; i >= 0; i--) {
                if (curr[i] != null && curr[i].value() == num) {
                    guard[i] = curr[i].next()[i];
                } else if (curr[i] != null && curr[i].next()[i] != null && curr[i].next()[i].value() == num) {
                    curr[i] = curr[i].next()[i];
                }
            }

//            if (node.next().length > currentLevel) {
//                currentLevel = node.next().length;
//            }

            return true;
        }

        private Node[] getNodeOf(int num, int level) {
            final var prev = new Node[level];
            var curr = guard;

            for (int i = currentLevel - 1; i >= 0; i--) {
                while (curr[i].next()[i] != null && curr[i].value() < num) {
                    curr = curr[i].next();
                }
                if (i < prev.length) {
                    prev[i] = curr[i];
                }
            }

            return prev;
        }

        private int getLevel() {
            int level = 1;
            while (random.nextBoolean() && level < MAX_LEVEL) {
                level++;
            }

            return level;
        }

        @Override
        public String toString() {
            final var builder = new StringBuilder(128);

            for (int i = currentLevel - 1; i >= 0; i--) {
                builder.append("lvl: ").append(i).append("=[");

                var curr = guard[i];
                while (curr != null) {
                    builder.append(curr.value).append(" -> ");
                    curr = curr.next()[i];
                }

                builder.append(". ] ");
            }

            return builder.toString();
        }
    }

    static Stream<Arguments> trivialExamples() {
        return Stream.of(
                of("\"Skiplist\", \"add\", \"add\", \"add\", \"search\", \"add\", \"search\", \"erase\", \"erase\", \"search\"",
                        "[], [1], [2], [3], [0], [4], [1], [0], [1], [1]",
                        "null, null, null, null, false, null, true, false, true, false"),
                of("\"Skiplist\",\"add\",\"add\",\"add\",\"search\",\"add\",\"search\",\"add\",\"search\",\"search\"",
                        "[],[1],[2],[3],[0],[4],[1],[5],[3],[6]",
                        "null,null,null,null,false,null,true,null,true,false"));
    }

    @ParameterizedTest
    @MethodSource("trivialExamples")
    public void shouldPassTrivialExamples(String calls, String params, String expectedOutputs) {
        // Given
        final var callsList = stream(calls.split(","))
                .map(String::trim)
                .filter(quatedString -> quatedString.length() >= 2 && quatedString.charAt(0) == '"' && quatedString.charAt(quatedString.length() - 1) == '"')
                .map(quotedString -> quotedString.substring(1, quotedString.length() - 1))
                .toList();
        final var paramsList = stream(params.split(","))
                .map(String::trim)
                .filter(brackets -> brackets.length() >= 2 && brackets.charAt(0) == '[' && brackets.charAt(brackets.length() - 1) == ']')
                .map(brackets -> brackets.substring(1, brackets.length() - 1))
                .toList();
        final var expectedList = stream(expectedOutputs.split(","))
                .map(String::trim)
                .map(Boolean::valueOf)
                .toList();
        // Test data assertions
        assertThat(callsList.size()).isEqualTo(paramsList.size()).isEqualTo(expectedList.size());

        Skiplist skiplist = null;
        for (int i = 0; i < callsList.size(); i++) {
            switch (callsList.get(i)) {
                case "Skiplist" -> skiplist = new Skiplist();
                case "add" -> Objects.requireNonNull(skiplist).add(Integer.parseInt(paramsList.get(i)));
                case "search" -> {
                    // When
                    final var actual = Objects.requireNonNull(skiplist).search(Integer.parseInt(paramsList.get(i)));

                    // Then
                    assertThat(actual)
                            .withFailMessage("Search failed for value: " + paramsList.get(i) + " (#" + i + " element)")
                            .isEqualTo(expectedList.get(i));
                }
                case "erase" -> {
                    // When
                    final var actual = Objects.requireNonNull(skiplist).erase(Integer.parseInt(paramsList.get(i)));

                    // Then
                    assertThat(actual)
                            .withFailMessage("Erase failed for value: " + paramsList.get(i) + " (#" + i + " element)")
                            .isEqualTo(expectedList.get(i));
                }
            }

        }
    }
}
